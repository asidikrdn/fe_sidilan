import { Container, Row, Col } from "react-bootstrap";

const Footer = () => {
  return (
    <footer className="bg-white position-absolute bottom-0 w-100">
      <Container>
        <Row className="py-3">
          <Col
            xs={12}
            lg={4}
            className="d-flex flex-wrap align-items-center h-100"
          >
            <div>
              <h1 className="fw-bold">SiDILAN</h1>
              <h3 className="d-lg-block d-none">
                Sistem Diagnosa <br /> Laptop & Notebook
              </h3>
            </div>
          </Col>
          <Col
            xs={12}
            lg={{ span: 4, offset: 4 }}
            className="d-flex flex-wrap justify-content-end align-items-center h-100"
          >
            <div className="text-end">
              <h1 className="d-lg-block d-none">sidilan.com</h1>
              <h5>
                Membantumu untuk mendiagnosa kerusakan pada Laptop/Notebook kamu
              </h5>
              <small>&copy; 2022 All Rights Reserved</small>
            </div>
          </Col>
        </Row>
      </Container>
    </footer>
  );
};

export default Footer;
