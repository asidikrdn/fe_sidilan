import { useEffect, useState, useContext } from "react";
import { Container, Row, Col, Form, Button, Table } from "react-bootstrap";
import { MyContext } from "../store/Store";
import {
  setPath,
  setTabelInputDiagnosa,
  setGejala,
  setBobot,
} from "../store/actions/actions";
import axios from "axios";

const Diagnosa = (props) => {
  const [state, dispatchState] = useContext(MyContext);

  const [input, setInput] = useState({
    id_gejala: "",
    id_bobot: "",
  }); // state penampung perubahan input saat input select gejala dan bobot dipilih
  const [dataKerusakan, setDataKerusakan] = useState([]); // state penampung data kerusakan hasil dari endpoint /diagnosa backend

  // const axios = require("axios");

  // membuat fungsi untuk mengambil data gejala dari backend
  const getGejala = async () => {
    // const response = await fetch("http://localhost:4135/api/gejala");
    const response = await axios("http://localhost:4135/api/gejala");
    // console.log(response);
    const data = response.data;
    dispatchState(setGejala(data));
  };

  // membuat fungsi untuk mengambil data bobot dari backend
  const getBobot = async () => {
    const response = await axios("http://localhost:4135/api/bobot");
    const data = response.data;
    dispatchState(setBobot(data));
  };

  // const authCheck = async () => {
  //   try {
  //     // const response = await fetch("http://localhost:4135/api/auth", {
  //     //   headers: {
  //     //     Authorization: `Bearer ${localStorage.getItem("token")}`,
  //     //   },
  //     // });
  //     const response = await axios({
  //       url: "http://localhost:4135/api/auth",
  //       headers: {
  //         Authorization: `Bearer ${localStorage.getItem("token")}`,
  //       },
  //     });
  //     if (response.status !== 200) {
  //       throw new Error(response.status);
  //     }
  //     dispatchState(setLoginState(true));
  //   } catch (e) {
  //     console.log(e);
  //     dispatchState(setLoginState(false));
  //   }
  // };

  useEffect(() => {
    dispatchState(setPath(window.location.pathname));
    // state.isLogin === false && authCheck();
    // authCheck();
    getGejala();
    getBobot();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // menghandle event change pada input gejala dan bobot
  const handleInputChange = (e) => {
    e.target.name === "bobotUser"
      ? setInput((prevState) => ({
          ...prevState,
          [e.target.name]: parseFloat(e.target.value),
        }))
      : setInput((prevState) => ({
          ...prevState,
          [e.target.name]: e.target.value,
        }));
  };

  // menghandle saat tombol 'tambah gejala' pada form diklik
  const handleTambahGejala = (e) => {
    e.preventDefault();

    if (
      state.tabelInputDiagnosa.some((el) => el.id_gejala === input.id_gejala)
    ) {
      let indexDataEdited = state.tabelInputDiagnosa.findIndex(
        (el) => el.id_gejala === input.id_gejala
      );

      let newTabelInputDiagnosa = [...state.tabelInputDiagnosa];

      newTabelInputDiagnosa.splice(indexDataEdited, 1, input);

      dispatchState(setTabelInputDiagnosa(newTabelInputDiagnosa));
    } else {
      let newTabelInputDiagnosa = [...state.tabelInputDiagnosa, input];
      dispatchState(setTabelInputDiagnosa(newTabelInputDiagnosa));
    }

    setInput((prevState) => ({ ...prevState, id_gejala: "", id_bobot: "" }));
  };

  const handleResetGejala = () => {
    dispatchState(setTabelInputDiagnosa([]));
  };

  const handleDiagnosa = async (inputData) => {
    // const response = await fetch("http://localhost:4135/api/diagnosa", {
    //   method: "POST",
    //   headers: {
    //     "Content-Type": "application/json",
    //   },
    //   body: JSON.stringify(inputData),
    // });
    const response = await axios({
      method: "POST",
      url: "http://localhost:4135/api/diagnosa",
      headers: {
        "Content-Type": "application/json",
      },
      data: JSON.stringify(inputData),
    });
    // console.log(response);
    const data = response.data;
    setDataKerusakan(data);
  };

  // console.log(input);
  // console.log(state.tabelInputDiagnosa);

  return (
    <>
      {/* <h1 className="text-center pt-3">Diagnosa Kerusakan Laptop</h1> */}

      <Container className="pt-4">
        <Row className="pb-2">
          <Col xs={12} lg={5}>
            <h3 className="text-center">Form Tambah Gejala</h3>
            <hr className="w-75 mx-auto" />
            <Form onSubmit={handleTambahGejala}>
              <Form.Group className="mb-3" controlId="formGejala">
                <Form.Label>Gejala :</Form.Label>
                <Form.Select
                  aria-label="Gejala"
                  name="id_gejala"
                  onChange={handleInputChange}
                  value={input.id_gejala}
                >
                  <option
                    value=""
                    // selected={input.id_gejala === "" ? true : false}
                  >
                    Pilih Gejala
                  </option>
                  ;
                  {state.gejala.map((el) => {
                    return (
                      <option value={el.id_gejala} key={el.id_gejala}>
                        {el.nama_gejala}
                      </option>
                    );
                  })}
                </Form.Select>
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBobot">
                <Form.Label>Bobot :</Form.Label>
                <Form.Select
                  aria-label="Bobot"
                  name="id_bobot"
                  onChange={handleInputChange}
                  disabled={input.id_gejala === "" ? true : false}
                  value={input.id_bobot}
                >
                  <option
                    value=""
                    // selected={input.id_bobot === "" ? true : false}
                  >
                    Pilih Bobot
                  </option>
                  ;
                  {state.bobot.map((el, i) => {
                    return (
                      <option value={el.id_bobot} key={i}>
                        {el.keterangan}
                      </option>
                    );
                  })}
                </Form.Select>
              </Form.Group>
              <p className="d-md-block d-none mb-1 opacity-0">Tombol Tambah</p>
              <Button
                variant="primary"
                type="submit"
                className="w-100"
                disabled={
                  input.id_gejala !== "" && input.id_bobot !== "" ? false : true
                }
              >
                Tambah Gejala
              </Button>
            </Form>
          </Col>
          <Col xs={12} lg={7} className="text-center mt-lg-0 mt-3">
            <h3 className="text-center">Tabel Gejala</h3>
            <hr className="w-75 mx-auto" />
            <Table
              striped
              bordered
              hover
              className="w-100 mx-auto"
              style={{ minHeight: "60%", maxHeight: "80%", overflow: "scroll" }}
            >
              <thead>
                <tr className="text-center" style={{ verticalAlign: "middle" }}>
                  <th className="w-75">Gejala Kerusakan</th>
                  <th className="w-25">Bobot</th>
                </tr>
              </thead>
              <tbody>
                {state.tabelInputDiagnosa.map((data) => {
                  return (
                    <tr className="text-center" key={data.id_gejala}>
                      <td>
                        {state.gejala.map((el) => {
                          return (
                            el.id_gejala === data.id_gejala && el.nama_gejala
                          );
                        })}
                      </td>
                      <td>
                        {state.bobot.map((el) => {
                          return el.id_bobot === data.id_bobot && el.keterangan;
                        })}
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
            <Button
              className="text-center mx-2"
              variant="success"
              onClick={() => {
                handleDiagnosa(state.tabelInputDiagnosa);
              }}
            >
              Cek Kerusakan
            </Button>
            <Button
              className="text-center mx-2"
              variant="warning"
              onClick={handleResetGejala}
            >
              Reset Gejala
            </Button>
          </Col>
        </Row>
        <Row className="mt-5 pt-5">
          <Col xs={12}>
            <h3 className="text-center">Hasil Diagnosa</h3>
            <hr className="w-100 mx-auto" />
          </Col>
          <Col xs={12} className="text-center">
            <Table striped bordered hover className="w-100 mx-auto">
              <thead>
                <tr className="text-center" style={{ verticalAlign: "middle" }}>
                  <th className="w-75">Jenis Kerusakan</th>
                  <th className="w-25">Nilai Kepastian (CF)</th>
                </tr>
              </thead>
              <tbody>
                {dataKerusakan.map((data, i) => {
                  return (
                    data.score_kerusakan !== 0 && (
                      <tr className="text-center" key={i}>
                        <td>{data.nama_kerusakan}</td>
                        <td>{Math.round(data.score_kerusakan)} %</td>
                      </tr>
                    )
                  );
                })}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Diagnosa;
