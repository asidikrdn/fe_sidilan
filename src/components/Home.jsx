import { useEffect, useContext, useState } from "react";
import { Container, Row, Col, Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import Header from "./Header";
import { MyContext } from "../store/Store";
import { setPath, setKerusakan, setLoginState } from "../store/actions/actions";
import axios from "axios";

const Home = () => {
  const [state, dispatchState] = useContext(MyContext);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    dispatchState(setPath(window.location.pathname));
    // state.isLogin === false && authCheck();
    authCheck();
    getKerusakan();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // console.log(state.isLogin);
  // console.log(state.path);

  const authCheck = async () => {
    try {
      const response = await axios({
        url: "http://localhost:4135/api/auth",
        method: "GET",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });
      if (response.status !== 200) {
        throw new Error(response.status);
      }
      dispatchState(setLoginState(true));
    } catch (e) {
      console.log(e);
      dispatchState(setLoginState(false));
    }
  };

  const getKerusakan = async () => {
    try {
      const res = await axios("http://localhost:4135/api/kerusakan");
      dispatchState(setKerusakan(res.data));
      // return json;
    } finally {
      setLoading(false);
    }
  };

  // console.log(props.kerusakan);

  return (
    <>
      <Header></Header>
      <Container className="py-4">
        <h4 className="ms-2">Daftar Kerusakan</h4>
        {loading ? (
          <h1 className="text-center">Loading...</h1>
        ) : (
          <Row>
            {state.kerusakan.map((el) => {
              return (
                <Col xs={12} lg={4} key={el.nama_kerusakan} className="py-2">
                  <Card
                    style={{ boxShadow: "0 2px 15px rgba(0, 0, 0, 0.4)" }}
                    className="h-100 rounded-4"
                  >
                    <Card.Img
                      variant="top"
                      src={el.img}
                      className="p-2 rounded-4"
                    />
                    <Card.Body>
                      <Link
                        style={{ textDecoration: "none" }}
                        to={`/kerusakan?id=${el.id_kerusakan}`}
                      >
                        <Card.Title>{el.nama_kerusakan}</Card.Title>
                      </Link>
                      <Card.Subtitle className="lead fs-6">
                        {el.atribut_kerusakan !== null
                          ? el.atribut_kerusakan.length + " Gejala"
                          : "Belum Ada Gejala"}
                      </Card.Subtitle>
                    </Card.Body>
                  </Card>
                </Col>
              );
            })}
          </Row>
        )}
      </Container>
    </>
  );
};

export default Home;
