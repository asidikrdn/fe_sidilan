import { useEffect, useState, useContext } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import { MyContext } from "../store/Store";
import { setPath, setKerusakan } from "../store/actions/actions";
import axios from "axios";

const Kerusakan = (props) => {
  const [state, dispatchState] = useContext(MyContext);
  const [detailKerusakan, setDetailKerusakan] = useState();
  const [loading, setLoading] = useState(false);

  // console.log(window.location.search.substring(4));

  useEffect(() => {
    dispatchState(setPath(window.location.pathname));
    // state.isLogin === false && authCheck();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (state.kerusakan.length > 0) {
      setDetailKerusakan(
        state.kerusakan.filter((el) => {
          return el.id_kerusakan === window.location.search.substring(4);
        })[0]
      );
    } else {
      setLoading(true);
      getKerusakan();
      setDetailKerusakan(
        state.kerusakan.filter((el) => {
          return el.id_kerusakan === window.location.search.substring(4);
        })[0]
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.kerusakan]);

  const getKerusakan = async () => {
    try {
      const res = await axios("http://localhost:4135/api/kerusakan");
      dispatchState(setKerusakan(res.data));
      // return json;
    } finally {
      setLoading(false);
    }
  };

  return (
    <>
      <Container className="pt-4">
        <Link
          className="d-inline-block my-3"
          style={{ textDecoration: "none" }}
          to="/"
        >
          <p
            id="home"
            // className={`nav-link mb-0 ${props.path === "/" && "active"}`}
          >
            Kembali
          </p>
        </Link>
        {loading ? (
          <h1 className="text-center">LOADING...</h1>
        ) : (
          <>
            <h1 className="text-center mb-4" id="projectTitle">
              {detailKerusakan !== undefined && detailKerusakan.nama_kerusakan}
            </h1>

            <Row>
              <Col xs={12} className="text-center">
                <img
                  src={
                    detailKerusakan !== undefined
                      ? detailKerusakan.img
                      : "false"
                  }
                  alt={
                    detailKerusakan !== undefined
                      ? detailKerusakan.nama_kerusakan
                      : "false"
                  }
                  className="img-fluid"
                />
              </Col>
              <Col xs={12} className="mt-5">
                <h5>
                  {detailKerusakan !== undefined &&
                  detailKerusakan.atribut_kerusakan !== null
                    ? "Kerusakan ini umumnya dapat diketaui dengan gejala sebagai berikut :"
                    : "Sementara ini, kami belum dapat mengetahui gejala yang umumnya menyebabkan kerusakan ini"}
                </h5>
                <ul>
                  {detailKerusakan !== undefined &&
                    detailKerusakan.atribut_kerusakan !== null &&
                    detailKerusakan.atribut_kerusakan.map((el, i) => {
                      return <li key={i}>{el.nama_gejala}</li>;
                    })}
                </ul>
                <p>
                  Tindakan yang diambil menyesuaikan dengan persentase kerusakan
                  berdasarkan gejala yang dirasakan. Untuk dapat mengetahui
                  tingkat persentase kerusakan, silahkan lakukan perhitungan
                  pada halaman diagnosa yang tersedia di aplikasi ini.
                </p>
                <p>
                  Jika hasil persentase yang didapatkan kurang dari 80%,
                  silahkan coba lakukan perawatan pada part terkait, tetapi,
                  jika persentase kerusakan sudah lebih dari 80% akan lebih baik
                  jika part tersebut diganti dengan part yang baru.
                </p>
              </Col>
            </Row>
          </>
        )}
      </Container>
    </>
  );
};

export default Kerusakan;
