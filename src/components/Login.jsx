import { useEffect, useState, useContext } from "react";
import { useNavigate } from "react-router-dom";
import { MyContext } from "../store/Store";
import { setPath, setLoginState } from "../store/actions/actions";
import axios from "axios";

const Login = () => {
  const [formInput, setFormInput] = useState({ email: "", password: "" });
  const navigate = useNavigate();
  const [state, dispatchState] = useContext(MyContext);

  const getToken = async (formData) => {
    const response = await axios({
      url: "http://localhost:4135/api/login",
      method: "POST",
      data: formData,
    });

    let data = await response.data;

    // console.log(response.status);
    // console.log(data);

    if (response.status === 200) {
      //set token on localStorage
      localStorage.setItem("token", data.token);
      dispatchState(setLoginState(true));
    }
  };

  const handleInputChange = (e) => {
    setFormInput({ ...formInput, [e.target.name]: e.target.value });
  };

  const handleLoginSubmit = (e) => {
    e.preventDefault();

    let formData = new FormData();

    formData.append("email", formInput.email);
    formData.append("password", formInput.password);

    getToken(formData);
  };

  useEffect(() => {
    dispatchState(setPath(window.location.pathname));

    if (state.isLogin === true) {
      navigate("/");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.isLogin]);

  return (
    <section
      id="project-form"
      className="d-flex align-items-center"
      style={{ height: "80vh" }}
    >
      <div className="container my-4">
        <h1 className="text-center py-3">Login to Your Account</h1>
        <div className="row">
          <div className="col-10 offset-1 col-lg-6 offset-lg-3">
            <form
              id="contactForm"
              className="w-100 mx-auto"
              method="post"
              onSubmit={(event) => {
                handleLoginSubmit(event);
              }}
            >
              <div className="mb-3">
                <label htmlFor="email" className="form-label h4">
                  Email
                </label>
                <input
                  type="email"
                  name="email"
                  id="email"
                  className="form-control"
                  onChange={(e) => {
                    handleInputChange(e);
                  }}
                />
              </div>
              <div className="mb-3">
                <label htmlFor="password" className="form-label h4">
                  Password
                </label>
                <input
                  type="password"
                  name="password"
                  id="password"
                  className="form-control"
                  onChange={(e) => {
                    handleInputChange(e);
                  }}
                />
              </div>
              <div className="w-100 d-flex justify-content-end">
                <button type="submit" className="btn btn-dark fs-5">
                  submit
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Login;
