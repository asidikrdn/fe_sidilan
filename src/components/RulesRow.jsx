import { Button } from "react-bootstrap";
import { useEffect, useState, useContext } from "react";
import { MyContext } from "../store/Store";
import { setGejala, setBobot } from "../store/actions/actions";
import axios from "axios";

const RulesRow = (props) => {
  const [state, dispatchState] = useContext(MyContext);
  const [input, setInput] = useState({
    id_aturan: props.id_aturan,
    id_gejala: "",
    id_bobot: "",
  });
  const [edit, setEdit] = useState(false);

  useEffect(() => {
    if (state.gejala.length <= 0) {
      getGejala();
    }

    if (state.bobot.length <= 0) {
      getBobot();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // membuat fungsi untuk mengambil data gejala dari backend
  const getGejala = async () => {
    const response = await axios("http://localhost:4135/api/gejala");
    const data = await response.data;
    dispatchState(setGejala(data));
  };

  // membuat fungsi untuk mengambil data bobot dari backend
  const getBobot = async () => {
    const response = await axios("http://localhost:4135/api/bobot");
    const data = await response.data;
    dispatchState(setBobot(data));
  };

  const handleInputChange = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };

  const handleEditCondition = (e) => {
    if (edit) {
      setEdit(false);
    } else {
      setEdit(true);

      let [gejala] = state.gejala.filter((el) => {
        return el.nama_gejala === props.nama_gejala;
      });
      // console.log("id_gejala :" + gejala.id_gejala);

      let [bobot] = state.bobot.filter((el) => {
        return el.nilai_bobot === props.nilai_bobot;
      });
      // console.log("id_bobot :" + bobot.id_bobot);

      setInput({
        ...input,
        id_gejala: gejala.id_gejala,
        id_bobot: bobot.id_bobot,
      });
    }
  };

  const pushDataEdited = async () => {
    const response = await axios({
      url: "http://localhost:4135/api/editrules",
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      data: JSON.stringify(input),
    });
    response.statusText === "OK" && alert("Data berhasil diedit");
    response.statusText === "OK" && props.getKerusakan();
    setEdit(false);
  };

  const deleteData = async (e) => {
    const response = await axios({
      url: "http://localhost:4135/api/deleterules",
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      data: JSON.stringify(e.target.id),
    });
    // console.log(response);
    response.statusText === "OK" && alert("Data berhasil dihapus");
    response.statusText === "OK" && props.getKerusakan();
  };

  // console.log(input);

  return (
    <>
      {edit ? (
        <tr>
          <td className="text-center" width="10%">
            {props.id_aturan}
          </td>
          {/* <td>{props.nama_gejala}</td> */}
          <td>
            <select
              className="w-100"
              name="id_gejala"
              onChange={handleInputChange}
            >
              {/* <option value="Gejala">Gejala</option> */}
              {state.gejala !== undefined &&
                state.gejala.map((el) => {
                  return (
                    <option
                      key={el.id_gejala}
                      value={el.id_gejala}
                      selected={
                        el.nama_gejala === props.nama_gejala ? true : false
                      }
                    >
                      {el.nama_gejala}
                    </option>
                  );
                })}
            </select>
          </td>
          {/* <td className="text-center">{props.nilai_bobot}</td> */}
          <td>
            <select
              className="w-100"
              name="id_bobot"
              onChange={handleInputChange}
            >
              {/* <option value="Bobot">Bobot</option> */}
              {state.bobot !== undefined &&
                state.bobot.map((el) => {
                  return (
                    <option
                      key={el.id_bobot}
                      value={el.id_bobot}
                      selected={
                        el.nilai_bobot === props.nilai_bobot ? true : false
                      }
                    >
                      {el.nilai_bobot.toFixed(1)}
                    </option>
                  );
                })}
            </select>
          </td>
          <td valign="middle">
            <div className="d-flex flex-nowrap align-items-center justify-content-center">
              <Button
                className="mx-1"
                variant="success"
                onClick={pushDataEdited}
              >
                Simpan
              </Button>
              <Button
                className="mx-1"
                variant="warning"
                onClick={handleEditCondition}
              >
                Batal
              </Button>
            </div>
          </td>
        </tr>
      ) : (
        <tr>
          <td className="text-center" width="10%">
            {props.id_aturan}
          </td>
          <td>{props.nama_gejala}</td>
          <td className="text-center">{props.nilai_bobot.toFixed(1)}</td>
          <td valign="middle">
            <div className="d-flex flex-nowrap align-items-center justify-content-center">
              <Button
                className="mx-1"
                variant="warning"
                onClick={handleEditCondition}
              >
                Edit
              </Button>
              <Button
                className="mx-1"
                variant="danger"
                id={props.id_aturan}
                onClick={deleteData}
              >
                Delete
              </Button>
            </div>
          </td>
        </tr>
      )}
    </>
  );
};

export default RulesRow;
