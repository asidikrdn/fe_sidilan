import { Routes, Route } from "react-router-dom";
import Diagnosa from "./Diagnosa";
import Home from "./Home";
import Kerusakan from "./Kerusakan";
import Login from "./Login";
import Rules from "./Rules";

const MainContent = () => {
  return (
    <main>
      <Routes>
        <Route path="/" element={<Home></Home>}></Route>
        <Route path="/kerusakan" element={<Kerusakan></Kerusakan>}></Route>
        <Route path="/diagnosa" element={<Diagnosa></Diagnosa>}></Route>
        <Route path="/login" element={<Login></Login>}></Route>
        <Route path="/aturan" element={<Rules></Rules>}></Route>
      </Routes>
    </main>
  );
};

export default MainContent;
