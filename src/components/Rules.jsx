import { useEffect, useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Container,
  Row,
  Col,
  Table,
  Button,
  Collapse,
  Form,
} from "react-bootstrap";
import RulesRow from "./RulesRow";
import { MyContext } from "../store/Store";
import { setPath, setLoginState, setKerusakan } from "../store/actions/actions";
import axios from "axios";

const Rules = () => {
  const [state, dispatchState] = useContext(MyContext);
  const [loading, setLoading] = useState(true);
  const [openFormGejala, setOpenFormGejala] = useState(false);
  const [openFormKerusakan, setOpenFormKerusakan] = useState(false);
  const [openFormAturan, setOpenFormAturan] = useState(false);
  const [newGejala, setNewGejala] = useState("");
  const [newKerusakan, setNewKerusakan] = useState({
    namaKerusakan: "",
    imgKerusakan: null,
  });
  const [newAturan, setNewAturan] = useState({
    id_kerusakan: "",
    id_gejala: "",
    id_bobot: "",
  });

  const handleInputGejalaChange = (e) => {
    setNewGejala(e.target.value);
  };

  const handleInputKerusakanChange = (e) => {
    if (e.target.name === "imgKerusakan") {
      setNewKerusakan((prevState) => ({
        ...prevState,
        [e.target.name]: e.target.files[0],
      }));
    } else {
      setNewKerusakan((prevState) => ({
        ...prevState,
        [e.target.name]: e.target.value,
      }));
    }
  };

  const handleInputAturanChange = (e) => {
    setNewAturan((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const navigate = useNavigate();

  useEffect(() => {
    dispatchState(setPath(window.location.pathname));
    authCheck();

    if (state.kerusakan.length <= 0) {
      getKerusakan();
    } else {
      setLoading(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.isLogin]);

  const authCheck = async () => {
    try {
      const response = await axios({
        url: "http://localhost:4135/api/auth",
        method: "GET",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      });

      if (response.status !== 200) {
        throw new Error(response.status);
      }
      dispatchState(setLoginState(true));
    } catch (e) {
      console.log(e);
      dispatchState(setLoginState(false));
      navigate("/login");
    }
  };

  const getKerusakan = async () => {
    try {
      setLoading(true);
      const res = await axios("http://localhost:4135/api/kerusakan");
      const json = await res.data;
      dispatchState(setKerusakan(json));
      // return json;
    } finally {
      setLoading(false);
    }
  };

  const pushNewGejala = async () => {
    const response = await axios({
      url: "http://localhost:4135/api/addgejala",
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      data: JSON.stringify(newGejala),
    });
    setNewGejala("");
    setOpenFormGejala(false);
    response.statusText === "OK" && alert("Berhasil menambahkan gejala baru");
    response.statusText === "OK" && getKerusakan();
  };

  const pushNewKerusakan = async () => {
    let formData = new FormData();

    formData.append("nama-kerusakan", newKerusakan.namaKerusakan);
    formData.append("img", newKerusakan.imgKerusakan);

    const response = await axios({
      url: "http://localhost:4135/api/addkerusakan",
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      data: formData,
    });
    setNewKerusakan({
      namaKerusakan: "",
      imgKerusakan: null,
    });
    setOpenFormKerusakan(false);
    response.statusText === "OK" &&
      alert("Berhasil menambahkan kerusakan baru");
    response.statusText === "OK" && getKerusakan();
  };

  const pushNewRules = async () => {
    const response = await axios({
      url: "http://localhost:4135/api/addrules",
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      data: JSON.stringify(newAturan),
    });
    setNewAturan({
      id_kerusakan: "",
      id_gejala: "",
      id_bobot: "",
    });
    setOpenFormAturan(false);
    response.statusText === "OK" && alert("Berhasil menambahkan aturan baru");
    response.statusText === "OK" && getKerusakan();
  };

  // console.log(state.kerusakan);

  // console.log(newGejala);
  // console.log(newKerusakan);
  // console.log(newAturan);
  // console.log(state.kerusakan);

  return (
    <>
      <Container className="py-4">
        <Row className="mb-3">
          <Col xs="12" lg="6">
            <h4 className="ms-2">Daftar Kerusakan</h4>
          </Col>
          <Col xs="12" lg="6" className="text-end">
            <Button
              variant="success"
              className="mx-2"
              onClick={() => {
                setOpenFormKerusakan(false);
                setOpenFormAturan(false);
                setOpenFormGejala(!openFormGejala);
              }}
              aria-controls="form-gejala"
              aria-expanded={openFormGejala}
            >
              <i className="bi bi-plus-circle me-1"></i> Gejala
            </Button>
            <Button
              variant="success"
              className="mx-2"
              onClick={() => {
                setOpenFormGejala(false);
                setOpenFormAturan(false);
                setOpenFormKerusakan(!openFormKerusakan);
              }}
              aria-controls="form-kerusakan"
              aria-expanded={openFormKerusakan}
            >
              <i className="bi bi-plus-circle me-1"></i> Kerusakan
            </Button>
            <Button
              variant="success"
              className="mx-2"
              onClick={() => {
                setOpenFormGejala(false);
                setOpenFormKerusakan(false);
                setOpenFormAturan(!openFormAturan);
              }}
              aria-controls="form-aturan"
              aria-expanded={openFormAturan}
            >
              <i className="bi bi-plus-circle me-1"></i> Aturan
            </Button>
          </Col>
          <Col>
            <Collapse in={openFormGejala}>
              <div id="form-gejala">
                <Form
                  className="w-50 mx-auto"
                  onSubmit={(e) => {
                    e.preventDefault();
                    pushNewGejala();
                  }}
                >
                  <Form.Group className="mb-3" controlId="formGejala">
                    <Form.Label>Gejala Baru :</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Masukkan gejala"
                      value={newGejala ? newGejala : ""}
                      onChange={handleInputGejalaChange}
                    />
                    {/* <Form.Text className="text-muted">
                      We'll never share your email with anyone else.
                    </Form.Text> */}
                  </Form.Group>

                  <div className="text-center">
                    <Button
                      variant="primary"
                      type="button"
                      onClick={pushNewGejala}
                    >
                      Tambahkan Gejala Baru
                    </Button>
                  </div>
                </Form>
              </div>
            </Collapse>
            <Collapse in={openFormKerusakan}>
              <div id="form-kerusakan">
                <Form
                  className="w-50 mx-auto"
                  onSubmit={(e) => {
                    e.preventDefault();
                    pushNewKerusakan();
                  }}
                >
                  <Form.Group className="mb-3" controlId="formKerusakan">
                    <Form.Label>Kerusakan Baru :</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Masukkan kerusakan"
                      name="namaKerusakan"
                      value={
                        newKerusakan.namaKerusakan
                          ? newKerusakan.namaKerusakan
                          : ""
                      }
                      onChange={handleInputKerusakanChange}
                    />
                    {/* <Form.Text className="text-muted">
                      We'll never share your email with anyone else.
                    </Form.Text> */}
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="formKerusakan">
                    <Form.Label>Gambar Sparepart :</Form.Label>
                    <Form.Control
                      type="file"
                      name="imgKerusakan"
                      value={
                        newKerusakan.imgKerusakan
                          ? newKerusakan.imgKerusakan
                          : ""
                      }
                      placeholder="Masukkan gambar part yang rusak"
                      onChange={handleInputKerusakanChange}
                    />
                    {/* <Form.Text className="text-muted">
                      We'll never share your email with anyone else.
                    </Form.Text> */}
                  </Form.Group>

                  <div className="text-center">
                    <Button
                      variant="primary"
                      type="button"
                      onClick={pushNewKerusakan}
                    >
                      Tambahkan Kerusakan Baru
                    </Button>
                  </div>
                </Form>
              </div>
            </Collapse>
            <Collapse in={openFormAturan}>
              <div id="form-aturan">
                <Form
                  className="w-50 mx-auto"
                  onSubmit={(e) => {
                    e.preventDefault();
                    pushNewRules();
                  }}
                >
                  <Form.Group className="mb-3" controlId="formKerusakan">
                    <Form.Label>Kerusakan :</Form.Label>
                    <Form.Select
                      aria-label="Kerusakan"
                      name="id_kerusakan"
                      onChange={handleInputAturanChange}
                      // defaultValue=""
                      value={newAturan.id_kerusakan}
                    >
                      <option
                        value=""
                        // selected={newAturan.id_kerusakan === "" ? true : false}
                      >
                        Pilih Kerusakan
                      </option>
                      ;
                      {state.kerusakan.map((el) => {
                        return (
                          <option value={el.id_kerusakan} key={el.id_kerusakan}>
                            {el.nama_kerusakan}
                          </option>
                        );
                      })}
                    </Form.Select>
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="formGejala">
                    <Form.Label>Gejala :</Form.Label>
                    <Form.Select
                      aria-label="Gejala"
                      name="id_gejala"
                      onChange={handleInputAturanChange}
                      disabled={newAturan.id_kerusakan === "" ? true : false}
                      // defaultValue=""
                      value={newAturan.id_gejala}
                    >
                      <option
                        value=""
                        // selected={newAturan.id_gejala === "" ? true : false}
                      >
                        Pilih Gejala
                      </option>
                      ;
                      {state.gejala.map((el) => {
                        return (
                          <option value={el.id_gejala} key={el.id_gejala}>
                            {el.nama_gejala}
                          </option>
                        );
                      })}
                    </Form.Select>
                  </Form.Group>
                  <Form.Group className="mb-3" controlId="formBobot">
                    <Form.Label>Bobot :</Form.Label>
                    <Form.Select
                      aria-label="Bobot"
                      name="id_bobot"
                      onChange={handleInputAturanChange}
                      disabled={newAturan.id_gejala === "" ? true : false}
                      // defaultValue=""
                      value={newAturan.id_bobot}
                    >
                      <option
                        value=""
                        // selected={newAturan.id_bobot === "" ? true : false}
                      >
                        Pilih Bobot
                      </option>
                      ;
                      {state.bobot.map((el, i) => {
                        return (
                          <option value={el.id_bobot} key={i}>
                            {el.keterangan}
                          </option>
                        );
                      })}
                    </Form.Select>
                  </Form.Group>

                  <div className="text-center">
                    <Button
                      variant="primary"
                      type="button"
                      disabled={
                        newAturan.id_kerusakan === "" ||
                        newAturan.id_gejala === "" ||
                        newAturan.id_bobot === ""
                          ? true
                          : false
                      }
                      onClick={pushNewRules}
                    >
                      Tambahkan Aturan Baru
                    </Button>
                  </div>
                </Form>
              </div>
            </Collapse>
          </Col>
        </Row>
        {loading ? (
          <h1 className="text-center mt-5">Loading...</h1>
        ) : (
          <Row>
            {state.kerusakan.map((el) => {
              return (
                <Col xs={12} lg={6} key={el.nama_kerusakan} className="py-2">
                  <h5 className="text-center">{el.nama_kerusakan}</h5>
                  <Table striped>
                    <thead>
                      <tr>
                        <th className="text-center">ID</th>
                        <th className="text-center">Nama Gejala</th>
                        <th className="text-center">Bobot</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      {el.atribut_kerusakan != null ? (
                        el.atribut_kerusakan.map((atr) => {
                          return (
                            <RulesRow
                              id_aturan={atr.id_aturan}
                              nama_gejala={atr.nama_gejala}
                              nilai_bobot={atr.nilai_bobot}
                              getKerusakan={getKerusakan}
                              key={atr.id_aturan}
                            ></RulesRow>
                          );
                        })
                      ) : (
                        <tr>
                          <td colSpan={3} className="text-center">
                            <h5>Belum ada aturan untuk kerusakan ini</h5>
                          </td>
                        </tr>
                      )}
                      {/* {el.atribut_kerusakan.map((atr) => {
                        return (
                          <RulesRow
                            id_aturan={atr.id_aturan}
                            nama_gejala={atr.nama_gejala}
                            nilai_bobot={atr.nilai_bobot}
                            getKerusakan={getKerusakan}
                          ></RulesRow>
                        );
                      })} */}
                    </tbody>
                  </Table>
                </Col>
              );
            })}
          </Row>
        )}
      </Container>
    </>
  );
};

export default Rules;
