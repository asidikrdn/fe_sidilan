import { Link } from "react-router-dom";
import { useContext } from "react";
import { MyContext } from "../store/Store";
import { setLoginState } from "../store/actions/actions";

const Navbar = () => {
  const [state, dispatchState] = useContext(MyContext);

  const handleLogout = () => {
    dispatchState(setLoginState(false));
    localStorage.removeItem("token");
  };

  // console.log(state.isLogin);

  return (
    <nav
      className={`navbar navbar-expand-lg navbar-dark bg-dark p-0 w-100 ${
        state.path === "/" && "bg-transparent"
      }`}
    >
      <div className="container-fluid bg-dark px-2 px-lg-5 py-3">
        <Link
          className={`navbar-brand`}
          style={{ textDecoration: "none" }}
          to="/"
        >
          <p id="brand" className="fs-3 fw-bold mb-0">
            SiDILAN
          </p>
        </Link>
        <button
          id="nav-toggle-button"
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav w-100 fw-bolder fs-5">
            <li className="nav-item mx-3">
              <Link style={{ textDecoration: "none" }} to="/">
                <p
                  id="home"
                  className={`nav-link mb-0 ${state.path === "/" && "active"}`}
                >
                  Home
                </p>
              </Link>
            </li>
            <li className="nav-item mx-3">
              <Link style={{ textDecoration: "none" }} to="/diagnosa">
                <p
                  id="diagnosa"
                  className={`nav-link mb-0 ${
                    state.path === "/diagnosa" && "active"
                  }`}
                >
                  Diagnosa
                </p>
              </Link>
            </li>
            {state.isLogin && (
              <li className="nav-item mx-3">
                <Link style={{ textDecoration: "none" }} to="/aturan">
                  <p
                    id="aturan"
                    className={`nav-link mb-0 ${
                      state.path === "/aturan" && "active"
                    }`}
                  >
                    Daftar Aturan
                  </p>
                </Link>
              </li>
            )}
          </ul>
          <ul className="navbar-nav w-100 fw-bolder fs-5 d-lg-flex justify-content-end align-items-center d-block">
            {state.isLogin ? (
              <>
                <li className="nav-item mx-3">
                  <h5 className="text-white d-none d-lg-inline-block me-3 my-0 p-0">
                    Halo, Admin
                  </h5>
                </li>
                <li className="nav-item mx-3">
                  {/* <Link style={{ textDecoration: "none" }} to="/logout"> */}
                  <p
                    id="login"
                    className=" d-lg-inline-block nav-link mb-0 fs-6 btn btn-danger text-white py-2 px-3 rounded-3"
                    onClick={handleLogout}
                  >
                    Logout
                  </p>
                  {/* </Link> */}
                </li>
              </>
            ) : (
              <li className="nav-item mx-3">
                <Link style={{ textDecoration: "none" }} to="/login">
                  <p
                    id="login"
                    className="nav-link mb-0 fs-6 btn btn-primary text-white py-2 px-3 rounded-3"
                  >
                    Login
                  </p>
                </Link>
              </li>
            )}
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
