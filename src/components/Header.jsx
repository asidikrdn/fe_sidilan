import { Container, Row, Col } from "react-bootstrap";

const Header = () => {
  return (
    <header id="header">
      <div className="dark-overlay">
        <Container className="w-100 h-100" fluid>
          <Row className="h-100">
            <Col
              xs={12}
              lg={8}
              className="text-white d-flex flex-column justify-content-center h-100 p-5"
            >
              <h1 className="display-6 fw-bold mb-3">
                Sistem Diagnosa Laptop & Notebook
              </h1>
              <p className="lead text-white fw-normal">
                Membantumu untuk mendiagnosa kerusakan pada Laptop/Notebook
                kamu, <br /> atau kamu juga bisa hanya sekedar melihat
                gejala-gejala yang mengindikasikan kerusakan tertentu
              </p>
            </Col>
            <Col
              lg={4}
              className="d-none d-lg-block h-100"
              id="header-side-img"
            >
              {/* <img src="/img/header-side-img.png" alt="side-img" className="h-100"/> */}
            </Col>
          </Row>
        </Container>
      </div>
    </header>
  );
};

export default Header;
