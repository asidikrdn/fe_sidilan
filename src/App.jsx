import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle";
import "bootstrap-icons/font/bootstrap-icons.css";
import { BrowserRouter as Router } from "react-router-dom";
import Footer from "./components/Footer";
import Navbar from "./components/Navbar";
import MainContent from "./components/MainContent";

const App = () => {
  return (
    <Router className="position-relative">
      <Navbar></Navbar>
      <MainContent></MainContent>
      <Footer></Footer>
    </Router>
  );
};

export default App;
