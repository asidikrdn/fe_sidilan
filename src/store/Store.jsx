import React, { createContext, useReducer } from "react";
import { initialState, reducer } from "./reducers/reducers";

export const MyContext = createContext();

const Store = (props) => {
  const [state, dispatchState] = useReducer(reducer, initialState);

  return (
    <MyContext.Provider value={[state, dispatchState]}>
      {props.children}
    </MyContext.Provider>
  );
};

export default Store;
