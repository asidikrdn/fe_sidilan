// nilai state awal
export const initialState = {
  isLogin: false,
  path: "/",
  gejala: [],
  bobot: [],
  kerusakan: [],
  tabelInputDiagnosa: [],
  inputRules: {
    id_aturan: "",
    id_gejala: "",
    id_bobot: "",
  },
};

export const reducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case "path/set":
      return {
        ...state,
        path: payload,
      };
    case "auth/setlogin":
      return {
        ...state,
        isLogin: payload,
      };
    case "setGejala":
      return {
        ...state,
        gejala: payload,
      };
    case "setBobot":
      return {
        ...state,
        bobot: payload,
      };
    case "setKerusakan":
      return {
        ...state,
        kerusakan: payload,
      };
    case "diagnosa/setInput":
      return {
        ...state,
        tabelInputDiagnosa: payload,
      };
    default:
      return {
        ...state,
      };
  }
};
