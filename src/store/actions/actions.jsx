export const setPath = (path) => {
  return {
    type: "path/set",
    payload: path,
  };
};

export const setLoginState = (loginState) => {
  return {
    type: "auth/setlogin",
    payload: loginState,
  };
};

export const setGejala = (gejala) => {
  return {
    type: "setGejala",
    payload: gejala,
  };
};

export const setBobot = (bobot) => {
  return {
    type: "setBobot",
    payload: bobot,
  };
};

export const setKerusakan = (kerusakan) => {
  return {
    type: "setKerusakan",
    payload: kerusakan,
  };
};

export const setTabelInputDiagnosa = (input) => {
  return {
    type: "diagnosa/setInput",
    payload: input,
  };
};
