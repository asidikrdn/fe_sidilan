# Front End Aplikasi Sistem Diagnosa Laptop dan Notebook

  Aplikasi Sistem Diagnosa Laptop dan Notebook ini menggunakan `React JS` sebagai stack utama, dan beberapa pustaka pihak ketiga yaitu `React Bootstrap` dan `React Router Dom`.

## Cara Menggunakan Aplikasi

Berikut cara menjalankan aplikasi ini di komputer lokal

- Pastikan sudah menginstall `Node JS` dan `Git SCM` di komputer anda
- Clone repository ini dengan menjalankan `git clone https://gitlab.com/asidikrdn/fe_sidilan` di terminal/cmd
- Masuk ke folder `fe_sidilan` lalu buka terminal dan jalankan perintah `npm install` untuk mengunduh seluruh pustaka yang dibutuhkan untuk menjalankan aplikasi
- Selanjutnya, jika proses instalasi pustaka sudah selesai, jalankan perintah `npm start` pada terminal untuk memulai aplikasi
- Aplikasi dapat di akses pada `http://localhost:3000` menggunakan web browser yang tersedia di komputer anda
